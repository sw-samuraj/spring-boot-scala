package cz.swsamuraj.boot

import org.springframework.web.bind.annotation.{GetMapping, RequestMapping, RestController}

@RestController
@RequestMapping(path = Array("/rest"))
class QuestionController {

  @GetMapping(path = Array("/question"))
  def getAnswer(): Answer = {
    new Answer("42")
  }
}
